#!/bin/bash -x

# Build docker images for all containers - useful when building images locally

WORKDIR="$(dirname $0)/../ztp"
BUILD_SCRIPT=build.sh
EC=0

cd $WORKDIR

CONTAINERS=$(find . -maxdepth 1 -type d ! -path . | xargs -I P basename P)

for C in $CONTAINERS
do
  BUILDER="${C}/${BUILD_SCRIPT}"
  if [ -s $BUILDER ]
  then
    cd $C
    /bin/bash $BUILD_SCRIPT
    EC=$(($EC + $?))
    cd -
  else
    echo "No ${BUILD_SCRIPT} for ${C} found in ${WORKDIR}/${C}, skipping..."
  fi
done

exit $EC
