import os
import sys
from jinja2 import Environment

env = Environment()
ext = ['.j2', '.jinja2']
dir = '.' if len(sys.argv) != 2 else sys.argv[1]

templates = []
for d, l in [(d, l) for d, _, l in os.walk(dir)]:
    templates += [os.path.join(d, f) for f in l if os.path.splitext(f)[-1] in ext]

for t in templates:
    print("Validating '{template}'...".format(template=t))
    with open(t, 'r') as file:
        env.parse(file.read())
