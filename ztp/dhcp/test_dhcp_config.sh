#!/bin/bash

DUMMY_ADDR=10.255.255.240/28
DUMMY_INTF=ethdhcptest

if ip link set dev $DUMMY_INTF
then
  echo "Interface ${DUMMY_INTF} already exists:"
  ip link show dev $DUMMY_INTF
  echo 'Exiting...'
  exit 1
else
  modprobe dummy
  ip link add $DUMMY_INTF type dummy
  ip addr add $DUMMY_ADDR dev $DUMMY_INTF
  ip link set $DUMMY_INTF up

  cp data/dhcpd.conf.sample data/dhcpd.conf
fi
